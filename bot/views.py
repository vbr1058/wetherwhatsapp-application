from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from twilio.twiml.messaging_response import MessagingResponse

from .query_weather import get_pnrStatus,generate_Pnr_Status,get_CoachPosition,generate_CoachPosition
from django.conf import settings


# @csrf_exempt
# def webhook(request):
#     response = MessagingResponse()
#     if request.method == "POST":
#         lat, lon = request.POST.get('Latitude'), request.POST.get('Longitude')
#         # print(request)
#         parameters = request.POST
#         print(parameters)
#         if lat and lon:
#             weather_response = get_weather(lat, lon, settings.OPEN_WEATHER_API_KEY)
            # message_body = generate_weather_message(weather_response)
#             response.message(message_body)
#         else:
#             response.message("Send your location")
#     return HttpResponse(response.to_xml(), content_type='text/xml')


# @csrf_exempt
# def webhook(request):
#     response = MessagingResponse()
#     if request.method == "POST":
#         message = request.POST.get("Body")
#         print(request)
#         print(message)
#         response.message('You said: ' + message)
#     return HttpResponse(response.to_xml(), content_type='text/xml')

# ==============================================================================================================================

# Below Code is working as expected
# @csrf_exempt
# def checkpnr(request):
#     response = MessagingResponse()
#     if request.method == "POST":
#         message = request.POST.get("Body")
#         print(request.POST)
#         if message:
#             pnrResult=get_pnrStatus(message)
#             pnrStatusBody=generate_Pnr_Status(pnrResult)
#             response.message(pnrStatusBody)
#         else:
#             response.message("Send your location")
#     return HttpResponse(response.to_xml(), content_type='text/xml')






@csrf_exempt
def trainDetails(request):
    response = MessagingResponse()
    if request.method == "POST":
        message = request.POST.get("Body")
        print(message)
        if message.find("PNR: ")!=-1:
            pnrResult=get_pnrStatus(''.join(filter(lambda i: i.isdigit(), message)))
            pnrStatusBody=generate_Pnr_Status(pnrResult)
            response.message(pnrStatusBody)

        elif message.find("getCoachPosition")!=-1:
            coachPosition=get_CoachPosition(''.join(filter(lambda i: i.isdigit(), message)))
            coachPositionBody=generate_CoachPosition(coachPosition)
            response.message(coachPositionBody)
        else:
            response.message("Send Proper Message as Follows:\n 1. PNR: XXX \n 2. getCoachPosition: Train Number")
    return HttpResponse(response.to_xml(), content_type='text/xml')











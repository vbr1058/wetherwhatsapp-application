import calendar
from datetime import datetime

import requests


def get_weather(lat, lon, api_key):
    weather_response = requests.get(
        f"https://api.openweathermap.org/data/2.5/onecall?lat"
        f"={lat}&lon={lon}&exclude=hourly&appid={api_key}&"
        f"units=metric").json()
    return weather_response


def generate_weather_message(weather_response):
    current_weather = {
        "temperature": int(weather_response["current"]["temp"]),
        "day_of_week": calendar.day_name[datetime.fromtimestamp(
            weather_response["current"]["dt"]).weekday()],
        "description":
            weather_response["current"]["weather"][0]["description"],
        "main":
            weather_response["current"]["weather"][0]["main"]

    }
    forecast_messages = []
    for daily_weather in weather_response["daily"]:
        max_temp = daily_weather["temp"]["max"]
        min_temp = daily_weather["temp"]["min"]
        weekday = calendar.day_name[datetime.fromtimestamp(
            daily_weather["dt"]).weekday()]
        description = daily_weather["weather"][0]["description"]
        forecast_message = f"{weekday}: {description}, " \
                           f"Low {max_temp}°C, High {min_temp}°C  \n"
        forecast_messages.append(forecast_message)
    forecast_messages = "\n".join(forecast_messages)
    message_body = f" *Current Weather* \n \n"\
                   f"{current_weather['day_of_week']}: "\
                   f"{current_weather['description']}, "\
                   f"{current_weather['temperature']} °C \n \n"\
                   f" *Forecast* \n \n"\
                   f"{forecast_messages}"

    return message_body






# =============================================================================================================


def get_pnrStatus(pnr):
    pnr_resp = requests.get("https://www.redbus.in/railways/api/getPnrData?pnrno="+pnr).json()
    # print(pnr_resp)
    return pnr_resp


def generate_Pnr_Status(pnr_response):
    pnrStatus = {
        "PNR Number": int(pnr_response["pnrNo"]),
        "overallStatus": str(pnr_response["overallStatus"]),
        "Train Number":str(pnr_response["trainNumber"]),
        "Train Name": str(pnr_response["trainName"])
    }
    return pnrStatus



def get_CoachPosition(trainNo):
    coachPositionResponse = requests.get("https://m.redbus.in/railways/api/getCoachPosition?trainNo="+trainNo).json()
    return coachPositionResponse



def generate_CoachPosition(coach_Position_response):
    coachPosition = {
        "Coach Positions": str(coach_Position_response["coachPosition"]) 
        }
    return coachPosition